#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <dlfcn.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

#define Record 11
#define Replay 22
#define Explore 33

#define Runnable_Thread 12
#define Busy_Thread     23
#define Terminated_Thread 45
#define Waiting_Thread 56
#define Exploration_Done 101
#define Deadlock_Found 202

int (*original_pthread_create)(pthread_t*, const pthread_attr_t*, void* (*)(void*), void*) = NULL;
//void (*original_pthread_exit)(void*) = NULL;
int (*original_pthread_join)(pthread_t, void**) = NULL;
int (*original_pthread_mutex_lock)(pthread_mutex_t*) = NULL;
int (*original_pthread_mutex_unlock)(pthread_mutex_t*) = NULL;

static pthread_t Current_Thread = 0;
static pthread_mutex_t Global_Lock = PTHREAD_MUTEX_INITIALIZER;
static void initialize_original_functions();
static map<pthread_t, int> Thread_Map;
static map<pthread_mutex_t*, pthread_t> Mutex_Map;

struct Thread_Arg {
    void* (*start_routine)(void*);
    void* arg;
};

struct Thread_Info {
    pthread_t tid;
    pthread_t wait_tid;
    int status;
    pthread_mutex_t * waiting_on;
    Thread_Info(pthread_t tid_, int status_, pthread_mutex_t * waiting_on_) {
        tid = tid_;
        status = status_;
        waiting_on = waiting_on_;
    }
};

static int mode;
static vector<Thread_Info> Thread_List;
static bool first_time = true;
static const char * TRACK_FILE = "track_file";
static const char * TRACK_FILE_COPY = "track_file_copy";
static ifstream track_file_in;
static ofstream track_file_out;
static int number_of_lines = 0;
static int current_position = 0;

static int write_count = 0;

int give_first_unlocked(int start) {
    //cout << "In give_first_unlocked, tid = " << pthread_self() << endl;
    for (int i = start; i < Thread_List.size(); i++) {
        // WE REMOVE THE CONDITION THAT THE THREAD TO BE FOUND SHOULD NOT BE EQUAL TO ITS OWN SELF.... Coz THIS
        // MIGHT BE A SIMPLE POSSIBILIY IN CASE OF SITUATIONS LIKE New Thread in thread_main !
        //cout << "In for loop, i = " << i << ", thread status = " << Thread_List[i].status << endl;
        if (Thread_List[i].status == Runnable_Thread)
    		return i;
    }
    //cout << "In give_first_unlocked, going to return -1" << endl;
    return -1;
}

bool all_dead() {
    for (vector<Thread_Info>::iterator it = Thread_List.begin(); it != Thread_List.end(); it++) {
        if ((*it).status == Runnable_Thread) {
            return false;
        }
    }
    //cout << "Seems all are dead" << endl;
    return true;
}

void write_choice(int choice) {
    track_file_out << choice << "\n";
    write_count++;
    //cout << "NUMBER OF TIMES WRITTEN TILL NOW : " << write_count << endl;
}

int read_choice() {
    string choice;
    getline(track_file_in, choice);
    current_position++;
    //cout << "current_position : " << current_position << "Read Value : " << choice << endl;
    return atoi(choice.c_str());
}

static
void choose_thread(void)
{
    int choice = -5;
    bool temm = true;
    //cout << "In choose_thread, mode = " << mode << ", tid = " << pthread_self() << endl;
    if (mode == Record) {
        choice = give_first_unlocked(0);
        if (choice != -1) {
            write_choice(choice);
            Current_Thread = Thread_List[choice].tid;
        } else {
            exit(Deadlock_Found);
        }
    } else if (mode == Replay) {
        choice = read_choice();
        // If after this only one more line then u should set the mode to explore.
        if (current_position == number_of_lines - 2) {
            //cout << "Replay, number_of_lines - 2 " << endl;
            mode = Explore;
            temm = false;
            write_choice(choice);
            Current_Thread = Thread_List[choice].tid;
        } else if (current_position == number_of_lines - 1) {
            //cout << "Replay, number_of_lines - 1" << endl;
            mode = Explore;
        } else {
            write_choice(choice);
            Current_Thread = Thread_List[choice].tid;
        }
        //Current_Thread = Thread_List[choice].tid;
    } 
    
    if (temm && mode == Explore) {
        // Mode is set to explore right now..
        // At the end of this else, set the mode to record !!
        // Coz we just explore one choice, after that it just becomes recording new states..
        //cout << "In Explore" << endl;
        if (choice == -5)
            choice = read_choice();

        choice = give_first_unlocked(choice + 1);
        if (choice != -1) {
            //cout << "choice != -1" << endl;
            write_choice(choice);
            mode = Record;
            Current_Thread = Thread_List[choice].tid;
        } else {
            //cout << "choice == -1" << endl;
            if (all_dead()) {
                //cout << "All DEAD !!" << endl;
                exit(Deadlock_Found);
            } else {
                // We are done with the exploration of this level.
                // There must not be any line after this.
                // Simply exit the program so that the earlier level could now be explored !!!
                //cout << "All ARE NOT DEAD !!" << endl;
                //cout << "Inside choose_thread, tid = " << pthread_self() << ", current_position : " << current_position << endl;
                if (current_position == 1) {// TODO
                    //cout << "ALL EXPLORATION DONE !!! Going to return " << Exploration_Done << endl;
                    exit(Exploration_Done);
                }
                //cout << "Exploration of this level done... Pruning current level, going to exploration of the upper level !!! " << endl;
                exit(0);
            }
        }
    }
}

static
void* thread_main(void *arg)
{
    //cout << "In thread_main, tid = " << pthread_self() << endl;
    while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
    original_pthread_mutex_lock(&Global_Lock);
    struct Thread_Arg thread_arg = *(struct Thread_Arg*)arg;
    free(arg);
    struct Thread_Info tinfo(pthread_self(), Runnable_Thread, NULL);
    Thread_List.push_back(tinfo);
    Thread_Map[pthread_self()] = Thread_List.size() - 1;
    choose_thread();

    //cout << "In thread_main, before second while, chosen thread = " << Current_Thread << ", tid = " << pthread_self() << endl;
    //cout << "Giving up lock !" << endl;
    original_pthread_mutex_unlock(&Global_Lock);
    //cout << "Lock Given Up !" << endl;
    while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
    //cout << "In thread_main, executing tid = " << pthread_self() << endl;
    original_pthread_mutex_lock(&Global_Lock);
    //cout << "Lock taken again..., Going to do thread_arg.start_routine(thread_arg.arg)" << endl;
    void *ret = thread_arg.start_routine(thread_arg.arg);
    //cout << "Line after thread_arg.start_routine(thread_arg.arg)" << endl;
    

    // Now the thread is basically exiting !
    // Following is the same work as we would have done in case of pthread_exit wrapper !
    for (vector<Thread_Info>::iterator it = Thread_List.begin(); it != Thread_List.end(); it++) {
        if ((*it).wait_tid == pthread_self()) {
            (*it).status = Runnable_Thread;
        }
    }

    Thread_List[Thread_Map[pthread_self()]].status = Terminated_Thread;
    choose_thread();
    original_pthread_mutex_unlock(&Global_Lock);
    return ret;
}

extern "C"
int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                   void *(*start_routine)(void*), void *arg)
{
    initialize_original_functions();

    struct Thread_Arg *thread_arg = (struct Thread_Arg*)malloc(sizeof(struct Thread_Arg));
    thread_arg->start_routine = start_routine;
    thread_arg->arg = arg;

    string first_line;
    //cout << "In create, tid = " << pthread_self() << endl;
	if (first_time) {
	    track_file_in.open(TRACK_FILE);
	    track_file_out.open(TRACK_FILE_COPY);
	    //cout << "In first_time of create, tid = " << pthread_self() << endl;
		first_time = false;
        getline(track_file_in, first_line);
        //cout << "File I/O result of getline(track_file_in, first_line) : " << first_line << ", tid = " << pthread_self() << endl;
        if (first_line.compare("RECORD") == 0) {
            mode = Record;
        } else {
            mode = Replay;
            ifstream inFile(TRACK_FILE);
            number_of_lines = count(istreambuf_iterator<char>(inFile), istreambuf_iterator<char>(), '\n');
            
            //cout << "Inside pthread_create, tid = " << pthread_self() << ", number_of_lines : " << number_of_lines << endl;
        }
        track_file_out << "REPLAY\n";
		struct Thread_Info tinfo(pthread_self(), Runnable_Thread, NULL);
		Thread_List.push_back(tinfo);
		Thread_Map[pthread_self()] = 0;
		original_pthread_mutex_lock(&Global_Lock);
	}

    int ret = original_pthread_create(thread, attr, thread_main, thread_arg);
    Current_Thread = *thread;
    //cout << "In pthread_create, going to give up lock" << endl;
    original_pthread_mutex_unlock(&Global_Lock);
    //cout << "In pthread_create, given up lock ! Going to while loop now.." << endl;
    while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
    //cout << "In pthread_create, came out of while loop now.. Trying to gain lock...  Current_Thread = " <<  Current_Thread << endl;
    original_pthread_mutex_lock(&Global_Lock);
    //cout << "In pthread_create, gained lock.. returning now.... !!  Current_Thread = " <<  Current_Thread << endl;

    return ret;
}

//extern "C"
//void pthread_exit(void *retval) {
//}

extern "C"
int pthread_join(pthread_t joinee, void **retval)
{
    initialize_original_functions();
    //cout << "In join, tid = " << pthread_self() << endl;
    if (Thread_List[Thread_Map[joinee]].status != Terminated_Thread) {
        //cout << "In join, non-terminated case, tid = " << pthread_self() << endl;
        Thread_List[Thread_Map[pthread_self()]].status = Waiting_Thread;
        Thread_List[Thread_Map[pthread_self()]].wait_tid = joinee;
        choose_thread();
        //cout << "In join, chosen thread = " << Current_Thread << ", tid = " << pthread_self() << endl;
        original_pthread_mutex_unlock(&Global_Lock);
        while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
        //cout << "In join, the other thread is DEAD! I AM tid = " << pthread_self() << endl;
        original_pthread_mutex_lock(&Global_Lock);
    }

    return original_pthread_join(joinee, retval);
}

extern "C"
int pthread_mutex_lock(pthread_mutex_t *mutex)
{
    initialize_original_functions();
 
    if (!Thread_List.empty()) {
        //cout << "In the non-empty case lock, tid = " << pthread_self() << endl;
        int index = Thread_Map[pthread_self()];
        // TODO Verify if the usage is right
        //Thread_List[index].waiting_on = mutex;
        //cout << "BEFORE" << endl;
        for (map<pthread_mutex_t*, pthread_t>::iterator it = Mutex_Map.begin(); it != Mutex_Map.end(); it++) {
            //cout << "Key : " << it->first << ", Value : " << it->second << endl;
        }
        if (Mutex_Map.find(mutex) == Mutex_Map.end()) {
            Thread_List[index].status = Runnable_Thread;
            //cout << "Mapping NOT Found !" << endl;
        } else {
            //cout << "Lock Already Taken !!!" << endl;
            Thread_List[index].status = Busy_Thread;
        }
        Thread_List[index].waiting_on = mutex;
        choose_thread();
        original_pthread_mutex_unlock(&Global_Lock);
        while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
        original_pthread_mutex_lock(&Global_Lock);
        Thread_List[index].waiting_on = NULL;

        Mutex_Map[mutex] = pthread_self();
        //cout << "AFTER" << endl;
        for (map<pthread_mutex_t*, pthread_t>::iterator it = Mutex_Map.begin(); it != Mutex_Map.end(); it++) {
            //cout << "Key : " << it->first << ", Value : " << it->second << endl;
        }
        
        //cout << "In Lock, going to set the other threads waiting on same mutex to busy" << endl;
        for (vector<Thread_Info>::iterator it = Thread_List.begin(); it != Thread_List.end(); it++) {
            if ((*it).tid != pthread_self() && (*it).waiting_on == mutex ) {
                //cout << "tid = " << (*it).tid << ", Thread Status = " << (*it).status << endl;
                (*it).status = Busy_Thread;
            }
        }
    } else {
        //cout << "In the empty case lock, tid = " << pthread_self() << endl;
        Mutex_Map[mutex] = pthread_self();
    }

    return original_pthread_mutex_lock(mutex);
}

extern "C"
int pthread_mutex_unlock(pthread_mutex_t *mutex)
{
    int ret;
    initialize_original_functions();
    if (!Thread_List.empty()) {
        //cout << "In the non-empty case unlock, tid = " << pthread_self() << endl;
        ret = original_pthread_mutex_unlock(mutex);
        Mutex_Map.erase(mutex);
        //cout << "In Unlock, going to set the other threads waiting on same mutex to runnable" << endl;
        for (vector<Thread_Info>::iterator it = Thread_List.begin(); it != Thread_List.end(); it++) {
            if ((*it).status == Busy_Thread && (*it).waiting_on == mutex) {
                //cout << "tid = " << (*it).tid << ", Thread Status = " << (*it).status << endl;
                (*it).status = Runnable_Thread;
            }
        }
    
        choose_thread();
    
        original_pthread_mutex_unlock(&Global_Lock);
        while (pthread_equal(Current_Thread, pthread_self()) == 0) {}
        original_pthread_mutex_lock(&Global_Lock);
    } else {
        //cout << "In the empty case unlock, tid = " << pthread_self() << endl;
        Mutex_Map.erase(mutex);
        ret = original_pthread_mutex_unlock(mutex);
    }
    return ret;
}

extern "C"
int sched_yield(void)
{
    // TODO

    return 0;
}

static
void initialize_original_functions()
{
    static bool initialized = false;
    if (!initialized) {
        initialized = true;

        original_pthread_create =
            (int (*)(pthread_t*, const pthread_attr_t*, void* (*)(void*), void*))dlsym(RTLD_NEXT, "pthread_create");
//        original_pthread_exit =
//            (void (*)(void*))dlsym(RTLD_NEXT, "pthread_exit");
        original_pthread_join =
            (int (*)(pthread_t, void**))dlsym(RTLD_NEXT, "pthread_join");
        original_pthread_mutex_lock =
            (int (*)(pthread_mutex_t*))dlsym(RTLD_NEXT, "pthread_mutex_lock");
        original_pthread_mutex_unlock =
            (int (*)(pthread_mutex_t*))dlsym(RTLD_NEXT, "pthread_mutex_unlock");
    }
}
