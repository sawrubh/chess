#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <dlfcn.h>
#include <stdio.h>
#include <unistd.h>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <sstream>
#include <sys/wait.h>
#include <unistd.h>

using namespace std;

static const char * TRACK_FILE = "track_file";
static const char * TRACK_FILE_COPY = "track_file_copy";
static const char * Target_Program;
static const int Exploration_Done = 101; // Just a random number.
static const int Deadlock_Found = 202;
static const int Segmentation_Found = 303;

int main(int argc, char *argv[]) {

    // Check that the arguments are correct.
    if (argc != 2 || !*argv[1]) {
        cout << "Arguments are invalid. Usage: ./chess_driver <target_program>" << endl;
        exit(-1);
    }
    Target_Program = argv[1];

    // Empty out the track file.
    ofstream file;
    file.open(TRACK_FILE, ios::out | ios::trunc );
    file << "RECORD\n";
    file.close();

    // Start the exploration.
    //string cmd = Command + " " + Target_Program;
    //const char * cmd_c = cmd.c_str();
    int return_val = -1;
    int counter = 0;
    int status = 0;
    char* env[] = {"LD_PRELOAD=./chess.so", NULL};
    do {
        cout << "IN DRIVER, GOING TO START THE EXECUTION OF THE PROGRAM !!" << endl;
        pid_t pid = fork();
        if (pid == 0) {
            execve(Target_Program, NULL, env);
        } else if (pid < 0) {
            cout << "Failed to fork" << endl;
            exit(1);
        } else {
            waitpid(pid, &status, 0);
            if(WIFEXITED(status)) {
                return_val = WEXITSTATUS(status);
                cout << "IN DRIVER, COMPLETED THE EXECUTION OF THE PROGRAM !! RETURN VALUE = " << return_val << endl;
         //       string nname = TRACK_FILE;
        //        ostringstream  temp;
      //          temp << counter;
  //              nname += temp.str();
    //            counter++;
//                int result = rename(TRACK_FILE, nname.c_str());
                if (remove(TRACK_FILE) != 0)
                    cout << "************ Error deleting file ***************" << endl;
                int result = rename(TRACK_FILE_COPY, TRACK_FILE);
                if (result != 0)
                    cout << "************ Error renaming file ************" << endl;
                cout << "\n\n************ Completed a run ************\n\n" << endl;
            } else {
                cout << "Segmentation Fault" << endl;
                return_val = Segmentation_Found;
            }
        }
    } while (return_val != Exploration_Done && return_val != Deadlock_Found && return_val != Segmentation_Found);

    if (return_val == Exploration_Done)
        cout << "\n\n************ You are so awesome ************\n\n" << endl;
    else if (return_val == Deadlock_Found)
        cout << "\n\n************ Oopsie, found a deadlock ************\n\n" << endl;
    else if (return_val == Segmentation_Found)
        cout << "\n\n************ Oopsie, found a segfault ************\n\n" << endl;

    return 0;
}
