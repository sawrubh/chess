CC=clang++

chess.so: chess.cpp
	@echo "Compiling chess.cpp..."
	$(CC) -o $@ -Wall -shared -g -O0 -D_GNU_SOURCE -fPIC -ldl $^

chessdriver: chessdriver.cpp
	@echo "Compiling CHESS driver..."
	$(CC) -o chessdriver chessdriver.cpp

example:
	@echo "Compiling sample..."
	gcc $(source).c -o $(source) -lpthread -lrt

test:
	make
	make chessdriver
	make example source=$(testcase)
	./chessdriver $(testcase)

clean:
	rm -f chess.so
	rm -f chessdriver
	rm -f sample1
	rm -f sample2
	rm -f sample3
	rm -f sample4
	rm -f sample5
	rm -f track_file*
