#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;

void* thread1(void* arg);
void* thread2(void* arg);

int *pointer;

int main()
{
    pthread_t thread;
    pthread_create(&thread, NULL, thread2, NULL);
    thread1(0);
    pthread_join(thread, NULL);
    
    return 0;
}

void* thread1(void *arg)
{
    pointer = (int *)malloc(sizeof(int));
}

void* thread2(void *arg)
{
    *pointer = 42;
}
