#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex3 = PTHREAD_MUTEX_INITIALIZER;

void* thread1(void* arg);
void* thread2(void* arg);
void* thread3(void*arg);

int main()
{
    pthread_t thread;
    pthread_t threadn;   
    pthread_create(&thread, NULL, thread2, NULL);
    pthread_create(&threadn, NULL, thread3, NULL);
    thread1(0);
    pthread_join(thread, NULL);
    pthread_join(threadn, NULL);   
    printf("EXIT \n");
    return 0;
}

void* thread1(void* arg)
{
    pthread_mutex_lock(&mutex1);   
    printf("lock 1-1\n");
    pthread_mutex_lock(&mutex2);
    printf("lock 1-2\n");

    pthread_mutex_unlock(&mutex2);
    pthread_mutex_unlock(&mutex1);
}

void* thread2(void* arg)
{
    pthread_mutex_lock(&mutex2);
    printf("lock 2-2\n");

    pthread_mutex_lock(&mutex3);
    printf("lock 2-3\n");
   
    pthread_mutex_unlock(&mutex3);            
    pthread_mutex_unlock(&mutex2);         
}

void* thread3(void* arg)
{
    pthread_mutex_lock(&mutex3);
    printf("lock 3-3\n");
    pthread_mutex_lock(&mutex1);
    printf("lock 3-1\n");

    pthread_mutex_unlock(&mutex1);            
    pthread_mutex_unlock(&mutex3);         
}
